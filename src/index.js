import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './scripts/components/Layout';
import { Router, Route } from 'react-router-dom';
import Post from './scripts/containers/Post';
import { Provider } from 'react-redux';
import { Store } from './scripts/store';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-quill/dist/quill.snow.css';
import history from './scripts/history';

ReactDOM.render(
  <Provider store={Store}>
    <Router history={history}>
      <Layout>
        <Route path="/posts" render={() => <Post operacao={0} />} />
        <Route
          path="/adicionar-post"
          render={({location}) => (
            <Post operacao={1} addEditPostProps={location.state} />
          )}
        />
        <Route
          path="/editar-post"
          render={({location}) => (
            <Post operacao={2} addEditPostProps={location.state} />
          )}
        />
      </Layout>
    </Router>
  </Provider>,
  document.getElementById('root')
);
