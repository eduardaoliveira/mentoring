import { createStore, applyMiddleware } from 'redux';
import { Reducers } from '../reducers';
import thunk from 'redux-thunk';
import { logger } from '../../logger';

export const Store = createStore(Reducers, applyMiddleware(thunk, logger));
    