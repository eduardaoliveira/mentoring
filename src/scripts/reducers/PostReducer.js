import typeToReducer from 'type-to-reducer';
import {
  ADD_POST,
  DELETE_POST,
  EDIT_POST,
  GET_POSTS
} from '../actions/actionTypes';

const initialState = {
  posts: {
    byId: {},
    allIds: []
  }
};

const arrayToObject = array =>
  array.reduce((obj, item) => {
    obj[item.id] = item;
    return obj;
  }, {});

export const PostReducer = typeToReducer(
  {
    [GET_POSTS]: (state, action) => ({
      ...state,
      posts: {
        byId: arrayToObject(action.payload),
        allIds: action.payload.map(post => post.id)
      }
    }),
    [ADD_POST]: (state, action) => ({
      ...state,
      posts: {
        byId: {
          ...state.posts.byId,
          [action.payload.id]: { ...action.payload }
        },
        allIds: [action.payload.id, ...state.posts.allIds]
      }
    }),
    [EDIT_POST]: (state, action) => ({
      ...state,
      posts: {
        byId: {
          ...state.posts.byId,
          [action.payload.id]: { ...action.payload }
        },
        allIds: [action.payload.id, ...state.posts.allIds]
      }
    }),
    [DELETE_POST]: (state, action) => ({
      ...state,
      posts: {
        byId: Object.values(state.posts.byId).reduce((obj, item) => {
          if(item.id !== action.payload.id){
            obj[item.id] = state.posts.byId[item.id];            
          }
          return obj;
        }, {}),
        allIds: state.posts.allIds.filter(id => id !== action.payload.id)
      }
    })
  },
  initialState
);
