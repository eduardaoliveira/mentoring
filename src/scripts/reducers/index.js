import { combineReducers } from 'redux';
import { PostReducer } from './PostReducer';

export const Reducers = combineReducers({
  PostReducer
});
