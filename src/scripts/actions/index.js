import { ADD_POST, DELETE_POST, EDIT_POST, GET_POSTS } from './actionTypes';
import APIUrls from '../APIUrls';
import WPAPI from 'wpapi';

const wp = new WPAPI({
  endpoint: 'http://claireholt.com.br/wp-json',
  username: 'dudaholt',
  password: 'eaif3era#',
  auth: true
});

export const getPosts = () => {
  return dispatch => {
    fetch(APIUrls.posts)
      .then(response => response.json())
      .then(data => {
        dispatch(posts(data));
      });
  };
};

export const createPost = post => {
  return dispatch => {
    wp.posts()
      .create({
        title: post.title,
        content: post.content,
        author: 1,
        status: 'publish'
      })
      .then(response => {
        console.log('response :', response);
        dispatch(
          add({
            id: response.id,
            title: { rendered: post.title },
            content: { rendered: post.content }
          })
        );
      });
  };
};

export const updatePost = post => {
  return dispatch => {
    wp.posts()
      .id(post.id)
      .update({
        title: post.title,
        content: post.content
      })
      .then(response => {
        console.log('response :', response);
        dispatch(
          edit({
            id: post.id,
            title: { rendered: post.title },
            content: { rendered: post.content }
          })
        );
      });
  };
};

export const removePost = post => {
  return dispatch => {
    wp.posts()
      .id(post.id)
      .delete()
      .then(response => {
        console.log('response :', response);
        dispatch(remove(post));
      });
  };
};

export const posts = value => ({
  type: GET_POSTS,
  payload: value
});

export const add = value => ({
  type: ADD_POST,
  payload: value
});

export const edit = value => ({
  type: EDIT_POST,
  payload: value
});

export const remove = value => ({
  type: DELETE_POST,
  payload: value
});
