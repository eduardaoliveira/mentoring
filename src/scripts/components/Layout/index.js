import React, { Component } from 'react';
import '../../../style/Layout.css';
import Sidebar from './Sidebar';

class Layout extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Sidebar />
          <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 ">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Layout;
