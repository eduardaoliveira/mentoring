import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => (
  <div className="col-sm-1 col-md-1 col-lg-1 col-xl-1" id="sidebar">
    <h3 className="text-center">CHBR Admin Lite</h3>
    <div className="nav">
      <Link to="/posts" className="nav-item">
        Notícias
      </Link>
    </div>
  </div>
);

export default Sidebar;
