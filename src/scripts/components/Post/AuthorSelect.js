import React from 'react';
import Select from 'react-select';
import { getAuthor, getPostsByAuthor } from '../../selectors/posts';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
];

export class AuthorSelect extends React.Component {
  state = {
    selectedOption: null
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    getAuthor(selectedOption);
    getPostsByAuthor(this.props.posts)
  };

  render() {
    return (
      <Select
        value={this.props.value}
        onChange={this.handleChange}
        options={this.props.options}
      />
    );
  }
}
