import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ReactQuill from 'react-quill';
import history from '../../history';

class AddEditPost extends Component {
  constructor() {
    super();

    this.state = {
      id: '',
      title: '',
      author: '',
      content: '',
      category: '',
      tags: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
  }

  componentDidMount() {
    if (this.props.post) {
      this.setState({
        id: this.props.post.id,
        title: this.props.post.title,
        author: this.props.post.author,
        content: this.props.post.content,
        category: this.props.post.category,
        tags: this.props.post.tags
      });
    } else {
      this.setState({
        author: 'Eduarda Oliveira'
      });
    }
    return;
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value.toUpperCase()
    });
  }

  handleContentChange(content) {
    this.setState({
      content
    });
  }

  clearInput() {
    this.setState({
      id: '',
      title: '',
      author: '',
      content: '',
      category: '',
      tags: ''
    });
  }

  render() {
    return (
      <div className="container">
        <div>
          <h3>{this.props.pageTitle}</h3>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="col-sm-10">
                <label htmlFor="title">Título</label>
                <input
                  type="text"
                  className="form-control"
                  id="title"
                  name="title"
                  value={this.state.title}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <label htmlFor="author">Autor</label>
                <input
                  type="text"
                  className="form-control"
                  id="author"
                  name="author"
                  value={this.state.author}
                  disabled={true}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <label htmlFor="content">Conteúdo</label>
                <ReactQuill
                  id="content"
                  value={this.state.content}
                  onChange={this.handleContentChange}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <label htmlFor="category">Categoria</label>
                <input
                  type="text"
                  className="form-control"
                  id="category"
                  name="category"
                  value={this.state.category}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <label htmlFor="tags">Tag</label>
                <input
                  type="text"
                  className="form-control"
                  id="tags"
                  name="tags"
                  value={this.state.tags}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    this.props.post
                      ? this.props.updatePost({
                          id: this.state.id,
                          title: this.state.title,
                          content: this.state.content,
                          author: this.state.author,
                          category: this.state.category,
                          tags: this.state.tags
                        })
                      : this.props.createPost({
                          title: this.state.title,
                          content: this.state.content
                        });

                    this.clearInput();
                    history.push('/posts');
                  }}
                >
                  Salvar
                </button>
                &nbsp;
                <Link to="/posts" className="btn btn-primary">
                  Cancelar
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddEditPost.propTypes = {
  pageTitle: PropTypes.string,
  post: PropTypes.object,
  posts: PropTypes.object,
  action: PropTypes.func
};

export default AddEditPost;
