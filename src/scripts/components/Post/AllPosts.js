import React from 'react';
import '../../../style/Layout.css';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
import { AuthorSelect } from './AuthorSelect';

const options = [
  { value: 'Jordana Bernar', label: 'Jordana Bernar' },
  { value: 'Eduarda Oliveira', label: 'Eduarda Oliveira' }
];

const AllPost = ({ data, columns }) => (
  <div className="container">
    <h3>Todos os posts</h3>
    <AuthorSelect options={options} posts={data} />
    <ReactTable
      data={data}
      columns={columns}
      defaultPageSize={10}
      minRows={3}
    />
    <br />
    <Link
      to={{
        pathname: '/adicionar-post',
        state: {
          pageTitle: 'Adicionar post'
        }
      }}
      className="btn btn-primary"
    >
      Adicionar novo
    </Link>
  </div>
);

AllPost.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.array
};

export default AllPost;
