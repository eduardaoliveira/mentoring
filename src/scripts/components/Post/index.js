import React, { Component } from 'react';
import '../../../style/Layout.css';
import AllPosts from './AllPosts';
import PropTypes from 'prop-types';
import AddEditPost from '../../containers/AddEditPost';
import { Link } from 'react-router-dom';
import { getPosts } from '../../selectors/posts';

class Post extends Component {
  constructor() {
    super();
    this.state = {
      columns: [
        {
          Header: 'Título',
          accessor: 'title'
        },
        {
          Header: 'Autor',
          accessor: 'author'
        },
        {
          Header: 'Categorias',
          accessor: 'category'
        },
        {
          Header: 'Tags',
          accessor: 'tags'
        },
        {
          Header: 'Ação',
          accessor: 'action',
          Cell: row => {
            return (
              <div className="text-center">
                <Link
                  to={{
                    pathname: '/editar-post',
                    state: {
                      post: row.original,
                      pageTitle: 'Editar post'
                    }
                  }}
                  className="btn btn-primary"
                >
                  Editar
                </Link>
                &nbsp;
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    this.deleteSelectedRow(row.original);
                  }}
                >
                  Excluir
                </button>
              </div>
            );
          }
        }
      ]
    };
  }

  isEquivalent(objA, objB) {
    var objAProps = Object.values(objA);
    var objBProps = Object.values(objB);
    

    if (objAProps.length !== objBProps.length) {
      return false;
    } else {
      var diferente = true;
      objAProps.forEach((elementA, index) => {
        if (
          elementA.title.rendered !== objBProps[index].title.rendered ||
          elementA.content.rendered !== objBProps[index].content.rendered
        ) {
           diferente = false;
        }
      });
      return diferente;
    }
  }

  componentDidMount() {
    this.props.getPosts();
  }

  shouldComponentUpdate(nextProps) {
    console.log(
      'this.isEquivalent(nextProps.posts.byId, this.props.posts.byId) :',
      this.isEquivalent(nextProps.posts.byId, this.props.posts.byId)
    );
    if (!this.isEquivalent(nextProps.posts.byId, this.props.posts.byId)) {
      return true;
    }
    return false;
  }

  mountTableData() {
    console.log('POSTAGENS :', this.props.posts.byId);
    return Object.values(this.props.posts.byId)
      .map(post => {
        return {
          id: post.id,
          title: post.title.rendered,
          author: post._embedded ? post._embedded.author['0'].name : '',
          content: post.content.rendered,
          category: post._embedded
            ? post._embedded['wp:term']['0']['0'].name
            : '',
          tags: post._embedded
            ? post._embedded['wp:term']['1']['0']
              ? post._embedded['wp:term']['1']['0'].name
              : ''
            : ''
        };
      })
      .reverse();
  }

  deleteSelectedRow(row) {
    this.props.removePost(row);
    this.forceUpdate();
  }

  //posts: op 0
  //adicionar-post: op 1
  //editar-post: op 2
  render() {
    return (
      <div className="container">
        {this.props.operacao === 0 ? (
          <AllPosts
            data={this.props.posts ? getPosts(this.props) : []}
            columns={this.state.columns}
            editPost={this.props.editPost}
          />
        ) : this.props.operacao === 1 ? (
          <AddEditPost pageTitle={this.props.addEditPostProps.pageTitle} />
        ) : this.props.operacao === 2 ? (
          <AddEditPost
            post={this.props.addEditPostProps.post}
            pageTitle={this.props.addEditPostProps.pageTitle}
          />
        ) : null}
      </div>
    );
  }
}

Post.propTypes = {
  addEditPostProps: PropTypes.object
};

export default Post;
