import { createSelector } from 'reselect';

const getPosts = state => {
  return Object.values(state.posts.byId)
    .map(post => {
      return {
        id: post.id,
        title: post.title.rendered,
        author: post._embedded ? post._embedded.author['0'].name : '',
        content: post.content.rendered,
        category: post._embedded
          ? post._embedded['wp:term']['0']['0'].name
          : '',
        tags: post._embedded
          ? post._embedded['wp:term']['1']['0']
            ? post._embedded['wp:term']['1']['0'].name
            : ''
          : ''
      };
    })
    .reverse();
};
const getPostsIds = state => state.posts.allIds;
const getAuthor = (author) => author;

const getPostsByAuthor = createSelector(
  [getPosts, getPostsIds, getAuthor],
  (posts, postsIds, author) => {
    return postsIds
      .filter(id => posts[id].author == author)
      .reduce((obj, id) => {
        obj[id] = posts[id];
        return obj;
      }, {});
  }
);

export { getPosts, getPostsIds, getPostsByAuthor, getAuthor };
