import { connect } from 'react-redux';
import { getPosts, removePost } from '../actions';
import Post from '../components/Post';

const mapStateToProps = store => ({
  posts: store.PostReducer.posts
});

const mapDispatchToProps = ({
  getPosts: () => getPosts(),
  removePost: (post) => removePost(post)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post);
