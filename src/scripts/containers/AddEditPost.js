import { connect } from 'react-redux';
import { createPost, updatePost } from '../actions';
import AddEditPost from '../components/Post/AddEditPost';

const mapStateToProps = store => ({
  posts: store.PostReducer.posts
});

const mapDispatchToProps = ({
  createPost: post => createPost(post),
  updatePost: post => updatePost(post)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEditPost);
