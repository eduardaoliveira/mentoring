# Plano de Atividades

## Apresentação

* **Nome**: Eduarda (Duda) Oliveira
* **Início do estágio**: 2018-10-29
* **Projeto**: Symphony | Email Integration
* **Mentor**: Joao Fernandes
* **Gerente**: Flavio Fernandez

### Objetivo

Treinar para a posição de desenvolvimento front-end no time Email Integration.

### Descrição

A Duda já tem conhecimento nas três linguagens triviais para desenvolvimento front-end (HTML, CSS e JS), já participou de projetos com as bibliotecas React e Flux e possui um site pessoal em produção.

Baseado nisso, o foco deste período de mentoração será aprender a biblioteca Redux, as bibliotecas do seu ecossistema e as convenções definidas pelo time EI-FE. Além disso, paralelamente, ela irá preencher possíveis lacunas e aperfeiçoar o quê já sabe.

### Lista de competências

| Código | Competência |
|--------|-------------|
| IC | Initiative and Commiment |
| TC | Technical Skill |
| WEQ | Work Efficiency and Quality |
| OP/PA | Organization, Planning and Pro-Activity |
| TW | Team Work |
| COM | Comunication |
| BM | Behavior and Maturity |
| AC | Analysis Capacity |
| ACH | Adaptability on Changes |
| E | English |

### Atividades recorrentes

| Atividade | Periodicidade | Descrição | Objetivo | Competências |
|-----------|---------------|-----------|----------|---------------|
| Planejamento de atividade | Início de atividades | Planejar o que será feito, estudado e apresentado. | Definir espectativas para o resultado final da atividade e antecipar possíveis obstáculos. | IC, OP/PA, TW, BM |
| Execução de atividade | Durante as atividade | Execução da atividade planejada. | Adquirir novos conhecimentos e aprimorar técnicas e competências já existentes. | IC, TC, WEQ, OP/PA, TW, COM, BM |
| Revisão de atividade | Final de atividades | Apresentação do que foi estudado e produzido. | Fixar conhecimentos e sanar dúvidas. | TW, COM, BM |
| Reunião | Diária | Conversa informal rápida para atualização dos pares no time, ou seja, o quê foi feito desde a última reunião. | Imersão nas práticas do SCRUM e cultura do time. | OP/PA, TW, COM, BM |
| Retrospectiva | Semanal | Revisão do que foi estudado e produzido durante a semana. | Evidenciar evolução técnica e pessoal. Revisitar plano de atividades, se necessário, para ajustá-lo. | OP/PA, TW, COM, BM, AC |
| - | Diária | Colaboração, comunicação, excelência e integridade (just be kind). | Absorver a cultura da empresa e empregá-la no dia a dia. | TW, COM, BM |
| - | Diária | Os materiais sugeridos para o aprendizado das competências técnicas serão em Inglês. | Imersão e aperfeiçoamento no idioma do cliente | E |
| - | - | Mudanças sutis ocorrerão propositalmente durante a execução de algumas atividades para simular alterações de projeto. | Estimular adaptação a mudanças, flexibilidade, solução de problemas e criatividade. | ACH |

## Conhecimentos técnicos almejados

* Bibliotecas
  * React
  * Redux
  * React Redux (react-redux)
* JavaScript
  * Map, Reduce e Filter
  * Promises
  * Imutalibidade
* React
  * Component vs PureComponent
  * Boas Práticas
* Redux
  * Reducer (type-to-reducer)
  * Redux Thunk (redux-thunk)
  * Middleware
  * Core implementation
  * Select e Reselect
  * Reducer Composition
  * Normalization
  * Boas Práticas
* Testes

## Atividades

| # | Atividade | Objetivo | Data |
|:-:|-----------|----------|-----:|
| <ul><li>- [x] 1</li></ul> | Ler sobre as bibliotecas *react*, *redux* e *react-redux*. | Compreender o quê cada biblioteca provê ao desenvolvimento, quando utilizá-as e, até mesmo, quando não utilizá-las. | 29/10/2018 |
| <ul><li>- [x] 2</li></ul> | Criar marcação base para o projeto CHBR Admin Lite. | Aperfeiçoar uso do React e praticar modularização e componentização. <br />Ter um projeto base para implementar e praticar os novos conhecimentos técnicos. | 05/11/2018 |
| <ul><li>- [x] 3</li></ul> | Trabalhar no ticket EI-1582. | Conhecer o produto *Symphony* e o projeto *Email Integration*. <br />Preparar o ambiente de desenvolvimento.  <br /> Entender a arquitetura do EI-FE.<br />Se adequar às convenções adotadas pelo time de FE. | 09/11/2018 |
| <ul><li>- [x] 4</li></ul> | Implementar comportamentos, *CRUD*, no CHBR AL. | Aprender sobre Redux, *react-redux containers*, *action creators* e planejamento de *store*. | 14/11/2018 <br />
| <ul><li>- [x] 5</li></ul> | Analisar e trocar possíveis componentes no CHBR AL. | Distinguir *Component*, *PureComponent* e *FunctionComponent* e entender quando usá-los. | 16/11/2018 |
| <ul><li>- [x] 6</li></ul> | Reimplementar *action creators* utilizando *redux-thunk* no CHBR AL. | Aprender sobre a biblioteca *redux-thunk*. | 19/11/2018 |
| <ul><li>- [x] 7</li></ul> | Reimplementar os *reducers* utilizando *type-to-reducer* no CHBR AL. | Aprender sobre a biblioteca *type-to-reducer*. <br />Endender que não é necessário se prender à forma como o Redux apresenta um recurso. | 21/11/2018 |
| <ul><li>- [x] 8</li></ul> | Implementar um *LoggerMiddleware* e listar as *actions* disparadas no CHBR AL. | Entender o que é um *middleware*, como é usado e implementado. | 21/11/2018 |
| <ul><li>- [x] 9</li></ul> | Normalizar a *store* no CHBR AL. | Entender os problemas de uma *deep store*. <br />Aprender sobre normalização e seus benefícios. | 27/11/2018 |
| <ul><li>- [ ] 10</li></ul> | Criar e usar selectors no CHBR AL. | Entender os problemas da *single store* no Redux. <br />Aprender sobre memoization. <br />Evitar problemas de performance. | |
| <ul><li>- [ ] 11</li></ul> | Implementar Redux *from scratch* e usá-lo no CHBR AL. | Esclarecer como de fato o Redux funciona. <br />Conseguir resolver problemas mais complicados. |  |
| <ul><li>- [ ] 12</li></ul> | Descobrir e implementar uma boa prática ainda não utilizada no CHBR AL. | Buscar novos conhecimentos. <br >Trazer novidades para o projeto. |  |
| <ul><li>- [x] 13</li></ul> | Ticket EI-1947. | Conhecer variáveis e extends usadas com scss.| 06/12/2018 |
| <ul><li>- [x] 14</li></ul> | Ticket EI-1934. | Entender como estão configurados os pacotes do projeto. <br />Entender como funciona e usar npm link. <br /> Desenvolver o perfil de depurador. | 10/12/2018 |
| <ul><li>- [x] 15</li></ul> | Ticket EI-1943. | Estudar o que são services dentro do projeto.<br /> Utilizar Symphony FE services.</br> Entender a lógica utilizada nos ID de elementos html. | 13/12/2018 |
| <ul><li>- [x] 16</li></ul> | Ticket EI-1986. | Trabalhar com definições diferentes de temas em um projeto.<br /> Entender hierarquia de folhas de estilo. | 13/12/2018 |
| <ul><li>- [x] 17</li></ul> | Ticket EI-1987. | Entender como módulos funcionam no projeto.<br /> Aprender aplicar barra de progresso com o tempo baseada na requisição. | 21/12/2018 
| <ul><li>- [x] 18</li></ul> | Ticket EI-1316. | Verificar funcionamento de criação de novas pastas no EI. | 26/12/2018 |
| <ul><li>- [x] 19</li></ul> | Ticket EI-2039. | Lidar com adaptações no projeto devido a update de bibliotecas. | 26/12/2018
| <ul><li>- [x] 20</li></ul> | Ticket EI-1645. | Entender como acontece um email server error.<br /> Simular um email server error.<br /> Verificar configurações de exibição de erro nos temas do projeto.<br /> Entrar em contato com Admin Console. | 27/12/2018 |
| <ul><li>- [x] 21</li></ul> | Ticket EI-1748. | Lidar com a integração de dois projetos (Popout e Email). | 28/12/2018
| <ul><li>- [x] 22</li></ul> | Ticket EI-2051. | Lidar com a lógica de paginação do EI. <br /> Entender sobre a utilização de Downshift no projeto EI. | 09/01/2018
| <ul><li>- [x] 23</li></ul> | Ticket EI-2000. | Lidar com atualização e manutenção de código depreciado. | 11/01/2018
| <ul><li>- [x] 24</li></ul> | Ticket EI-1561. | Estudar e adquirir boas práticas de desenvolvimento. <br /> Treinar o processo necessário para setar o ambiente de desenvolvimento do projeto. | 15/01/2018
| <ul><li>- [x] 24</li></ul> | Ticket ACME-5918. | | 23/01/2018
| <ul><li>- [x] 24</li></ul> | Ticket ACME-5937. | | 25/01/2018
| <ul><li>- [x] 24</li></ul> | Ticket ACME-5905. | | 29/01/2018
